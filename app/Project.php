<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
	use SoftDeletes;
	public $timestamps = true;
    protected $translatable = ['projects'];
    protected $fillable = ['project_name','team_lead_id','developer_id'];
}
