<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryGenerator extends Model
{
    use SoftDeletes;
	public $timestamps = true;
    protected $translatable = ['delivery_generators'];
    protected $fillable = ['project_id','user_id','file_path','revision_note'];
}
