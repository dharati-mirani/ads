<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Role extends \TCG\Voyager\Models\Role implements AuditableContract
{
	use \OwenIt\Auditing\Auditable;
	use SoftDeletes;
}