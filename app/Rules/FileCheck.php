<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
class FileCheck implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
       
        $allowedExts = array("ipa","apk");
        $extension = $value[0]->getClientOriginalExtension();

        $videoFormatCheck = (in_array($extension, $allowedExts)) ? true : false;

        return $videoFormatCheck;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid File.';
    }
}
