<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Notifications\SendDeliveryReminderEmailsNotification;
use App\DeliveryReminder;
use App\ProjectWiseDeveloper;
use App\User;

class DeliveryReminderController extends Controller
{

    /**
     * This function is for sending an email of project delivery reminder
    */
    public static function sendDeliveryReminderEmail()
    {
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $deliveryReminders = DeliveryReminder::select('delivery_reminders.*', 'project_wise_developers.*', 'projects.*')
                                  ->join('project_wise_developers', 'project_wise_developers.project_id', '=', 'delivery_reminders.project_id')
                                  ->join('projects', 'projects.id', '=', 'delivery_reminders.project_id')
                                  ->where('reminder_time', $now)
                                  ->get()->toArray();
        foreach ($deliveryReminders as $key => $value) {
            $user = User::where('id', $value['user_id'])->first();
            $user['projectName'] = $value['project_name'];
            $user['reminderNote'] = $value['note'];
            $user->notify(new SendDeliveryReminderEmailsNotification($user));
        }
    }
    
}
