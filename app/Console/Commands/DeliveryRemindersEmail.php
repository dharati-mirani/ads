<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\DeliveryReminderController;

class DeliveryRemindersEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DeliveryReminders:SendEmails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron will send a email of Delivery Reminders.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DeliveryReminderController::sendDeliveryReminderEmail();
    }
}
