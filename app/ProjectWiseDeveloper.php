<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectWiseDeveloper extends Model
{
    public $timestamps = true;
    protected $translatable = ['project_wise_developers'];
    protected $fillable = ['project_id','user_id'];
}
