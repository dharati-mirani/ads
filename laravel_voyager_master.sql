-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2019 at 03:15 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_voyager_master`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(2, NULL, 1, 'Category 2', 'category-2', '2019-10-02 06:01:51', '2019-10-02 06:01:51');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 5),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 9),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{}', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 1, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 6),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 1, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 0, 0, 0, 0, 0, '{}', 6),
(57, 1, 'first_name', 'text', 'First Name', 0, 1, 1, 1, 1, 1, '{}', 3),
(58, 1, 'last_name', 'text', 'Last Name', 0, 1, 1, 1, 1, 1, '{}', 4),
(60, 1, 'technology', 'select_dropdown', 'Technology', 0, 1, 1, 1, 1, 1, '{\"options\":{\"ios\":\"IOS\",\"android\":\"Android\"}}', 7),
(61, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(62, 9, 'project_name', 'text', 'Project Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(63, 9, 'team_lead_id', 'text', 'Team Lead Id', 0, 1, 1, 1, 1, 1, '{}', 3),
(64, 9, 'developer_id', 'text', 'Developer Id', 0, 0, 0, 0, 0, 0, '{}', 4),
(65, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 5),
(66, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(67, 9, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 7),
(68, 9, 'project_belongsto_user_relationship', 'relationship', 'Team Lead', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"team_lead_id\",\"key\":\"id\",\"label\":\"first_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(70, 9, 'project_belongstomany_user_relationship', 'relationship', 'Developers', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"first_name\",\"pivot_table\":\"project_wise_developers\",\"pivot\":\"1\",\"taggable\":\"0\"}', 10),
(71, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(72, 12, 'project_id', 'text', 'Project Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(73, 12, 'user_id', 'text', 'Team Lead Name', 1, 1, 1, 1, 1, 1, '{}', 3),
(74, 12, 'file_path', 'file', 'File Path', 1, 1, 1, 1, 1, 1, '{}', 4),
(75, 12, 'revision_note', 'text_area', 'Revision Note', 1, 1, 1, 1, 1, 1, '{}', 5),
(76, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(77, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(78, 12, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 8),
(79, 12, 'delivery_generator_belongsto_project_relationship', 'relationship', 'Project Name', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Project\",\"table\":\"projects\",\"type\":\"belongsTo\",\"column\":\"project_id\",\"key\":\"id\",\"label\":\"project_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2),
(80, 12, 'delivery_generator_belongsto_user_relationship', 'relationship', 'Team Lead Name', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"first_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(81, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(82, 13, 'project_id', 'text', 'Project Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(83, 13, 'note', 'text_area', 'Note', 1, 1, 1, 1, 1, 1, '{}', 3),
(84, 13, 'reminder_time', 'timestamp', 'Reminder Time', 1, 1, 1, 1, 1, 1, '{}', 4),
(85, 13, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 5),
(86, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(87, 13, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 7),
(88, 13, 'delivery_reminder_belongsto_project_relationship', 'relationship', 'Project', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Project\",\"table\":\"projects\",\"type\":\"belongsTo\",\"column\":\"project_id\",\"key\":\"id\",\"label\":\"project_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 2);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2019-10-02 06:01:50', '2019-10-03 00:02:13'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(9, 'projects', 'projects', 'Project', 'Projects', NULL, 'App\\Project', NULL, '\\App\\Http\\Controllers\\Voyager\\ProjectsController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-10-02 07:20:55', '2019-10-03 01:57:49'),
(12, 'delivery_generators', 'delivery-generators', 'Delivery Generator', 'Delivery Generators', NULL, 'App\\DeliveryGenerator', NULL, 'App\\Http\\Controllers\\voyager\\DeliveryGeneratorsController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-10-03 03:39:14', '2019-10-03 07:54:11'),
(13, 'delivery_reminders', 'delivery-reminders', 'Delivery Reminder', 'Delivery Reminders', NULL, 'App\\DeliveryReminder', NULL, '\\App\\Http\\Controllers\\Voyager\\DeliveryRemindersController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-10-04 01:15:25', '2019-10-04 01:45:45');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_generators`
--

CREATE TABLE `delivery_generators` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `file_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revision_note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_generators`
--

INSERT INTO `delivery_generators` (`id`, `project_id`, `user_id`, `file_path`, `revision_note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 5, '[{\"download_link\":\"delivery-generators\\\\October2019\\\\6tM9xFlagM7n92a6vbJD.apk\",\"original_name\":\"Sample Android App Test_v1.0_apkpure.com.apk\"}]', 'Testind this', '2019-10-04 00:45:32', '2019-10-04 00:45:32', NULL),
(2, 2, 9, '[{\"download_link\":\"delivery-generators\\\\October2019\\\\byrfC6MTmhlNBAIa0zAW.apk\",\"original_name\":\"Sample Android App Test_v1.0_apkpure.com.apk\"}]', 'Why this', '2019-10-04 00:49:05', '2019-10-04 00:49:05', NULL),
(3, 2, 9, '[{\"download_link\":\"delivery-generators\\\\October2019\\\\K6cEh9LVVfVLXxe8TH8D.apk\",\"original_name\":\"Sample Android App Test_v1.0_apkpure.com.apk\"}]', 'Why this', '2019-10-04 00:50:59', '2019-10-04 00:50:59', NULL),
(4, 2, 9, '[{\"download_link\":\"delivery-generators\\\\October2019\\\\Pt72S7gZ6Y71Empy6xZO.apk\",\"original_name\":\"Sample Android App Test_v1.0_apkpure.com.apk\"}]', 'Why this', '2019-10-04 00:53:07', '2019-10-04 00:53:07', NULL),
(5, 2, 9, '[{\"download_link\":\"delivery-generators\\\\October2019\\\\5T38X3b2ZckRoHIRaU4J.apk\",\"original_name\":\"Sample Android App Test_v1.0_apkpure.com.apk\"}]', 'Why this', '2019-10-04 00:56:17', '2019-10-04 00:56:17', NULL),
(6, 2, 9, '[{\"download_link\":\"delivery-generators\\\\October2019\\\\0J3hQjJNSi5uqN4eyleM.apk\",\"original_name\":\"Sample Android App Test_v1.0_apkpure.com.apk\"}]', 'Why this', '2019-10-04 00:56:27', '2019-10-04 00:56:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_reminders`
--

CREATE TABLE `delivery_reminders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` bigint(20) UNSIGNED NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `reminder_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_reminders`
--

INSERT INTO `delivery_reminders` (`id`, `project_id`, `note`, `reminder_time`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Demo Reminder', '2019-10-04 10:12:00', '2019-10-04 01:47:48', '2019-10-04 01:47:48', NULL),
(2, 2, 'Testing Project', '2019-10-04 10:08:00', '2019-10-04 02:03:46', '2019-10-04 02:03:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-10-02 06:01:50', '2019-10-02 06:01:50');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-10-02 06:01:50', '2019-10-02 06:01:50', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 7, '2019-10-02 06:01:50', '2019-10-04 04:43:13', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2019-10-02 06:01:50', '2019-10-02 06:01:50', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-10-02 06:01:50', '2019-10-02 06:01:50', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 11, '2019-10-02 06:01:50', '2019-10-04 04:43:07', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2019-10-02 06:01:50', '2019-10-03 00:47:03', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2019-10-02 06:01:50', '2019-10-04 04:43:07', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2019-10-02 06:01:50', '2019-10-04 04:43:07', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2019-10-02 06:01:50', '2019-10-04 04:43:07', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 12, '2019-10-02 06:01:50', '2019-10-04 04:43:07', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 10, '2019-10-02 06:01:51', '2019-10-04 04:43:07', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 8, '2019-10-02 06:01:51', '2019-10-04 04:43:10', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 9, '2019-10-02 06:01:51', '2019-10-04 04:43:10', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2019-10-02 06:01:51', '2019-10-04 04:43:07', 'voyager.hooks', NULL),
(15, 1, 'Projects', '', '_self', 'voyager-ticket', '#000000', NULL, 4, '2019-10-02 07:20:55', '2019-10-03 01:15:39', 'voyager.projects.index', 'null'),
(16, 1, 'Delivery Generators', '', '_self', 'voyager-ticket', '#000000', NULL, 5, '2019-10-03 03:39:14', '2019-10-04 04:43:28', 'voyager.delivery-generators.index', 'null'),
(17, 1, 'Delivery Reminders', '', '_self', 'voyager-ticket', '#000000', NULL, 6, '2019-10-04 01:15:25', '2019-10-04 04:43:46', 'voyager.delivery-reminders.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(21, '2017_08_05_000000_add_group_to_settings_table', 1),
(22, '2017_11_26_013050_add_user_role_relationship', 1),
(23, '2017_11_26_015000_create_user_roles_table', 1),
(24, '2018_03_11_000000_add_user_settings', 1),
(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
(26, '2018_03_16_000000_make_settings_value_nullable', 1),
(28, '2019_10_02_115836_update_users_table', 2),
(30, '2019_10_02_124519_create_projects_table', 3),
(33, '2019_10_03_070817_create_project_wise_developers_table', 4),
(34, '2019_10_03_073048_delete_developer_column_from_projects_table', 5),
(35, '2019_10_03_090026_create_delivery_generators_table', 6),
(36, '2019_10_04_063943_create_delivery_reminders_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2019-10-02 06:01:51', '2019-10-02 06:01:51');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(2, 'browse_bread', NULL, '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(3, 'browse_database', NULL, '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(4, 'browse_media', NULL, '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(5, 'browse_compass', NULL, '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(6, 'browse_menus', 'menus', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(7, 'read_menus', 'menus', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(8, 'edit_menus', 'menus', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(9, 'add_menus', 'menus', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(10, 'delete_menus', 'menus', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(11, 'browse_roles', 'roles', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(12, 'read_roles', 'roles', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(13, 'edit_roles', 'roles', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(14, 'add_roles', 'roles', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(15, 'delete_roles', 'roles', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(16, 'browse_users', 'users', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(17, 'read_users', 'users', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(18, 'edit_users', 'users', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(19, 'add_users', 'users', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(20, 'delete_users', 'users', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(21, 'browse_settings', 'settings', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(22, 'read_settings', 'settings', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(23, 'edit_settings', 'settings', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(24, 'add_settings', 'settings', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(25, 'delete_settings', 'settings', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(26, 'browse_categories', 'categories', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(27, 'read_categories', 'categories', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(28, 'edit_categories', 'categories', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(29, 'add_categories', 'categories', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(30, 'delete_categories', 'categories', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(31, 'browse_posts', 'posts', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(32, 'read_posts', 'posts', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(33, 'edit_posts', 'posts', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(34, 'add_posts', 'posts', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(35, 'delete_posts', 'posts', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(36, 'browse_pages', 'pages', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(37, 'read_pages', 'pages', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(38, 'edit_pages', 'pages', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(39, 'add_pages', 'pages', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(40, 'delete_pages', 'pages', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(41, 'browse_hooks', NULL, '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(42, 'browse_projects', 'projects', '2019-10-02 07:20:55', '2019-10-02 07:20:55'),
(43, 'read_projects', 'projects', '2019-10-02 07:20:55', '2019-10-02 07:20:55'),
(44, 'edit_projects', 'projects', '2019-10-02 07:20:55', '2019-10-02 07:20:55'),
(45, 'add_projects', 'projects', '2019-10-02 07:20:55', '2019-10-02 07:20:55'),
(46, 'delete_projects', 'projects', '2019-10-02 07:20:55', '2019-10-02 07:20:55'),
(47, 'browse_delivery_generators', 'delivery_generators', '2019-10-03 03:39:14', '2019-10-03 03:39:14'),
(48, 'read_delivery_generators', 'delivery_generators', '2019-10-03 03:39:14', '2019-10-03 03:39:14'),
(49, 'edit_delivery_generators', 'delivery_generators', '2019-10-03 03:39:14', '2019-10-03 03:39:14'),
(50, 'add_delivery_generators', 'delivery_generators', '2019-10-03 03:39:14', '2019-10-03 03:39:14'),
(51, 'delete_delivery_generators', 'delivery_generators', '2019-10-03 03:39:14', '2019-10-03 03:39:14'),
(52, 'browse_delivery_reminders', 'delivery_reminders', '2019-10-04 01:15:25', '2019-10-04 01:15:25'),
(53, 'read_delivery_reminders', 'delivery_reminders', '2019-10-04 01:15:25', '2019-10-04 01:15:25'),
(54, 'edit_delivery_reminders', 'delivery_reminders', '2019-10-04 01:15:25', '2019-10-04 01:15:25'),
(55, 'add_delivery_reminders', 'delivery_reminders', '2019-10-04 01:15:25', '2019-10-04 01:15:25'),
(56, 'delete_delivery_reminders', 'delivery_reminders', '2019-10-04 01:15:25', '2019-10-04 01:15:25');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(2, 1),
(2, 3),
(2, 4),
(3, 1),
(3, 3),
(3, 4),
(4, 1),
(4, 3),
(4, 4),
(5, 1),
(5, 3),
(5, 4),
(6, 1),
(6, 3),
(6, 4),
(7, 1),
(7, 3),
(7, 4),
(8, 1),
(8, 3),
(8, 4),
(9, 1),
(9, 3),
(9, 4),
(10, 1),
(10, 3),
(10, 4),
(11, 1),
(11, 3),
(11, 4),
(12, 1),
(12, 3),
(12, 4),
(13, 1),
(13, 3),
(13, 4),
(14, 1),
(14, 3),
(14, 4),
(15, 1),
(15, 3),
(15, 4),
(16, 1),
(16, 3),
(16, 4),
(17, 1),
(17, 3),
(17, 4),
(18, 1),
(18, 3),
(18, 4),
(19, 1),
(19, 3),
(19, 4),
(20, 1),
(20, 3),
(20, 4),
(21, 1),
(21, 3),
(21, 4),
(22, 1),
(22, 3),
(22, 4),
(23, 1),
(23, 3),
(23, 4),
(24, 1),
(24, 3),
(24, 4),
(25, 1),
(25, 3),
(25, 4),
(26, 1),
(26, 3),
(26, 4),
(27, 1),
(27, 3),
(27, 4),
(28, 1),
(28, 3),
(28, 4),
(29, 1),
(29, 3),
(29, 4),
(30, 1),
(30, 3),
(30, 4),
(31, 1),
(31, 3),
(31, 4),
(32, 1),
(32, 3),
(32, 4),
(33, 1),
(33, 3),
(33, 4),
(34, 1),
(34, 3),
(34, 4),
(35, 1),
(35, 3),
(35, 4),
(36, 1),
(36, 3),
(36, 4),
(37, 1),
(37, 3),
(37, 4),
(38, 1),
(38, 3),
(38, 4),
(39, 1),
(39, 3),
(39, 4),
(40, 1),
(40, 3),
(40, 4),
(41, 1),
(41, 3),
(41, 4),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-10-02 06:01:51', '2019-10-02 06:01:51');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_lead_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `team_lead_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Project Demo', 5, '2019-10-03 01:58:28', '2019-10-03 01:58:28', NULL),
(2, 'project test', 9, '2019-10-03 02:03:12', '2019-10-03 02:03:12', NULL),
(3, 'Test', 5, '2019-10-03 02:06:09', '2019-10-03 02:06:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_wise_developers`
--

CREATE TABLE `project_wise_developers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_wise_developers`
--

INSERT INTO `project_wise_developers` (`id`, `project_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 6, NULL, NULL),
(2, 1, 7, NULL, NULL),
(3, 2, 8, NULL, NULL),
(4, 2, 10, NULL, NULL),
(5, 3, 6, NULL, NULL),
(6, 3, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Administrator', '2019-10-02 06:01:50', '2019-10-02 06:09:47'),
(2, 'user', 'Normal User', '2019-10-02 06:01:50', '2019-10-02 06:01:50'),
(3, 'Team Lead', 'Team Lead', '2019-10-02 06:09:15', '2019-10-02 07:09:13'),
(4, 'Developer', 'Developer', '2019-10-02 06:09:28', '2019-10-02 06:09:28');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2019-10-02 06:01:51', '2019-10-02 06:01:51'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2019-10-02 06:01:51', '2019-10-02 06:01:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `technology` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `first_name`, `last_name`, `technology`) VALUES
(4, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$tYBCnnGJqBhsR8r5Hp7JEOH1UBMMQk7.mPtmPooy19vczwTJDk1vy', '1r0i7ljsrHRbVvpQcua6hfZvbCdmsc2jWpiicpPMKfByuUHe8QsDqDJA33RW', '{\"locale\":\"en\"}', '2019-10-02 06:01:51', '2019-10-02 06:55:47', 'fadmin', 'ladmin', 'None'),
(5, 3, 'Dipak Patel', 'dipak@yopmail.com', 'users/default.png', NULL, '$2y$10$Ny7eJjlcP/JJJA3Q8X0QpeyOW1zV5rKPLbaLpls0dzsKDs1s7NEqi', NULL, '{\"locale\":\"en\"}', '2019-10-02 23:41:46', '2019-10-02 23:56:15', 'Dipak', 'Patel', 'android'),
(6, 4, 'Nidhi Patel', 'nidhi@yopmail.com', 'users/default.png', NULL, '$2y$10$/uk841vwAVLWQG5WNV1gseeWTfN26/emoRQY7buB25S.M9Z4bmh.u', NULL, '{\"locale\":\"en\"}', '2019-10-02 23:52:34', '2019-10-02 23:52:34', 'Nidhi', 'Patel', 'android'),
(7, 4, 'Vrunda Parikh', 'vrunda@yopmail.com', 'users/default.png', NULL, '$2y$10$meBsnPaAX7HKbsuY6m0uMOQYLkHqiqvgalvHnqCeOQjRdqsuF6pdO', NULL, '{\"locale\":\"en\"}', '2019-10-02 23:53:16', '2019-10-02 23:53:16', 'Vrunda', 'Parikh', 'android'),
(8, 4, 'Dipshikha', 'vadhare@yopmail.com', 'users/default.png', NULL, '$2y$10$xCo0cQ6j.73A5DncEnc6huOg/bxPfRpV8Ri3i4ocBbVQssOWwdfpq', NULL, '{\"locale\":\"en\"}', '2019-10-02 23:53:57', '2019-10-02 23:55:57', 'Dipshikha', 'Vadhre', 'android'),
(9, 3, 'Parag Khalas', 'parag@yopmail.com', 'users/default.png', NULL, '$2y$10$HlUVrdTFzAn8MXKg4DHxYO9ELFGo5JKsCDBdSuTXNetL9cmWRCTAy', NULL, '{\"locale\":\"en\"}', '2019-10-02 23:54:46', '2019-10-02 23:55:46', 'Parag', 'Khalas', 'ios'),
(10, 4, 'Harsh', 'harsh@yopmail.com', 'users/default.png', NULL, '$2y$10$.XPgW.hjJugwrNUxV0DMEOuigThnt0WXZmpsOLo2.Qyr7pPTXKXu2', NULL, '{\"locale\":\"en\"}', '2019-10-02 23:55:31', '2019-10-02 23:55:31', 'Harsh', 'Panshuriya', 'ios'),
(11, 4, 'Dharti', 'dharti@yopmail.com', 'users/default.png', NULL, '$2y$10$lUEyFIyfGBXcEZhsaT7LU.DQ4ec.atMrR9uhNGXxcFIpCk5zxLZNi', NULL, '{\"locale\":\"en\"}', '2019-10-02 23:57:06', '2019-10-03 00:05:49', 'Dharti', 'Mirani', 'ios');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `delivery_generators`
--
ALTER TABLE `delivery_generators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_reminders`
--
ALTER TABLE `delivery_reminders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_wise_developers`
--
ALTER TABLE `project_wise_developers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `delivery_generators`
--
ALTER TABLE `delivery_generators`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `delivery_reminders`
--
ALTER TABLE `delivery_reminders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_wise_developers`
--
ALTER TABLE `project_wise_developers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
